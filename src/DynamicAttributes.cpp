//=============================================================================
// DynamicAttributes.cpp
//=============================================================================
// abstraction.......Modbus Data Viewer generic acces to Modbus devices
// class.............DynamicAttribute
// original author...N.Leclercq - SOLEIL
//=============================================================================

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// THIS IS NOT USED - THIS IS NOT USED - THIS IS NOT USED - THIS IS NOT USED 
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "DynamicAttributes.h"
#include <yat4tango/PropertyHelper.h>

namespace mdv
{
// ============================================================================
// Config::Config
// ============================================================================
DynamicModbusAttribute::Config::Config ()
{
  m_name            = "Not Initialised";
  m_rw              = Tango::READ;
  m_type            = 0;
  m_ratio           = 1.;
  m_modbus_address  = 1;
  m_hwp             = 0;
	m_host            = 0;
  m_register_type   = 0;
  m_nb_connection_retry = -1;
}

DynamicModbusAttribute::Config::Config (const Config & _src)
{
	*this = _src;
}
// ============================================================================
// Config::operator =
// ============================================================================
void DynamicModbusAttribute::Config::operator = (const Config & _src)
{
  m_name            = _src.m_name;
  m_rw              = _src.m_rw;
  m_type            = _src.m_type;
  m_ratio           = _src.m_ratio;
  m_modbus_address  = _src.m_modbus_address;
  m_hwp             = _src.m_hwp;
	m_host            = _src.m_host;
	m_register_type   = _src.m_register_type;
  m_nb_connection_retry = _src.m_nb_connection_retry;
}

// ======================================================================
// DynamicAttribute::DynamicAttribute
// ======================================================================
DynamicAttribute::DynamicAttribute (yat::SharedPtr<HWProxy> hwp, Tango::DeviceImpl * host)
 : Tango::LogAdapter(host), m_hwp(hwp)
{
  //- noop
}

// ======================================================================
// DynamicAttribute::~DynamicAttribute
// ======================================================================
DynamicAttribute::~DynamicAttribute ()
{
  //- noop
}

// ======================================================================
// DynamicModbusAttribute::DynamicModbusAttribute
// ======================================================================
DynamicModbusAttribute::DynamicModbusAttribute (const Config& _cfg)
 : DynamicAttribute (_cfg.m_hwp, _cfg.m_host),
   Tango::Attr (_cfg.m_name.c_str(),  _cfg.m_type, _cfg.m_rw),
   cfg (_cfg)
{
  //- TODO : insert calls to set attribute properties like format, min, max...
  //- control register type must be 0 (holding register) or 1 (input register)
  cfg.m_register_type = cfg.m_register_type != 0 ? 1 : 0;
}

// ======================================================================
// DynamicModbusAttribute::~DynamicModbusAttribute
// ======================================================================
DynamicModbusAttribute::~DynamicModbusAttribute ()
{
  //- noop
}

// ============================================================================
// DynamicModbusAttribute::read
// ============================================================================
void DynamicModbusAttribute::read (Tango::DeviceImpl *, Tango::Attribute &attr)
{    
  if (cfg.m_hwp == 0)
  {
    return;
  }

  DEBUG_STREAM << "DynamicModbusAttribute::read <-" << std::endl;
  if (this->cfg.m_hwp->get_com_state() == mdv::HWP_COMMUNICATION_ERROR  
      && (cfg.m_nb_connection_retry != -1 && cfg.m_hwp->get_com_error() >= cfg.m_nb_connection_retry))
  {
	  std::string reason = "It's not allowed to read attribute " + cfg.m_name;
    Tango::Except::throw_exception("TANGO_DEVICE_ERROR",
                                  reason.c_str(),
                                  "DynamicModbusAttribute::read()");
  }

  switch (cfg.m_type)
  {
  case Tango::DEV_BOOLEAN :
    {
      DEBUG_STREAM << "DynamicModbusAttribute::read DEV_BOOLEAN<-" << std::endl;
      db = cfg.m_hwp->get_bool_value(cfg.m_modbus_address);
      attr.set_value(& db);
      break;
    }
  case Tango::DEV_SHORT :
    {
      DEBUG_STREAM << "DynamicModbusAttribute::read DEV_SHORT<-" << std::endl;
      tmp = cfg.m_hwp->get_register_value(cfg.m_modbus_address, cfg.m_register_type);
      double res = static_cast<double>(tmp) * cfg.m_ratio;
      ds = static_cast<Tango::DevShort> (res);
      attr.set_value(& ds);
      break;
    }
  case Tango::DEV_LONG :
    {
      tmp = cfg.m_hwp->get_register_value(cfg.m_modbus_address, cfg.m_register_type);
      double res = static_cast<double>(tmp) * cfg.m_ratio;
      dl = static_cast<Tango::DevLong> (res);
      attr.set_value(& dl);
/*DEBUG_STREAM << "DynamicModbusAttribute::read DEV_LONG modbus address [" 
              << cfg.m_modbus_address 
              << "] register_type ["
              << cfg.m_register_type
              << "] ratio ["
              << cfg.m_ratio
              << "] tmp = " << tmp << " res = " << res << " dl = " << dl
              << std::endl;*/
      break;
    }
  case Tango::DEV_USHORT :
    {
      DEBUG_STREAM << "DynamicModbusAttribute::read DEV_USHORT<-" << std::endl;
      tmp = cfg.m_hwp->get_register_value(cfg.m_modbus_address, cfg.m_register_type);
      double res = static_cast<double>(tmp) * cfg.m_ratio;
      dus = static_cast <Tango::DevUShort> (res);
      attr.set_value(& dus);
      break;
    }
  case Tango::DEV_ULONG :
    {
      DEBUG_STREAM << "DynamicModbusAttribute::read DEV_ULONG<-" << std::endl;
      tmp = cfg.m_hwp->get_register_value(cfg.m_modbus_address, cfg.m_register_type);
      double res = static_cast<double>(tmp) * cfg.m_ratio;
      dul = static_cast <Tango::DevULong> (res);
      attr.set_value(& dul);
      break;
    }
  case Tango::DEV_FLOAT :
    {
      DEBUG_STREAM << "DynamicModbusAttribute::read DEV_FLOAT<-" << std::endl;
      tmp = cfg.m_hwp->get_register_value(cfg.m_modbus_address, cfg.m_register_type);
      df = static_cast<Tango::DevFloat>(tmp) * cfg.m_ratio;
      attr.set_value(& df);
      break;
    }
  case Tango::DEV_DOUBLE :
    {
      DEBUG_STREAM << "DynamicModbusAttribute::read DEV_DOUBLE<-" << std::endl;
      tmp = cfg.m_hwp->get_register_value(cfg.m_modbus_address, cfg.m_register_type);
      dd = static_cast<Tango::DevDouble>(tmp) * cfg.m_ratio;
      attr.set_value(& dd);
      break;
    }
  default : 
    DEBUG_STREAM << "DynamicModbusAttribute::read default<-" << std::endl;
    break;
  }
}

// ============================================================================
// DynamicModbusAttribute::write
// ============================================================================
void DynamicModbusAttribute::write (Tango::DeviceImpl *, Tango::WAttribute &attr)
{
  DEBUG_ASSERT(cfg.m_hwp != 0);
  
  if (this->cfg.m_hwp->get_com_state() == mdv::HWP_COMMUNICATION_ERROR  
      && (cfg.m_nb_connection_retry != -1 && cfg.m_hwp->get_com_error() >= cfg.m_nb_connection_retry))
  {
    std::string reason = "It's not allowed to write attribute " + cfg.m_name;
    Tango::Except::throw_exception("TANGO_DEVICE_ERROR",
                                  reason.c_str(),
                                  "DynamicModbusAttribute::write()");
  }

  switch (cfg.m_type)
  {
  case Tango::DEV_BOOLEAN :
    {
    bool src;
    attr.get_write_value(src);
    cfg.m_hwp->set_bool_value(cfg.m_modbus_address, src);
    break;
    }
  case Tango::DEV_SHORT :
    {
    Tango::DevShort src;
    attr.get_write_value(src);
    double tmp = static_cast<double>(src) / cfg.m_ratio; 
    cfg.m_hwp->set_register_value(cfg.m_modbus_address, static_cast<Tango::DevUShort>(tmp));
    break;
    }
  case Tango::DEV_LONG :
    {
    Tango::DevLong src;
    attr.get_write_value(src);
    double tmp = static_cast<double>(src) / cfg.m_ratio; 
    cfg.m_hwp->set_register_value(cfg.m_modbus_address, static_cast<Tango::DevUShort>(tmp));
    break;
    }
  case Tango::DEV_USHORT :
    {
    Tango::DevUShort src;
    attr.get_write_value(src);
    double tmp = static_cast<double>(src) / cfg.m_ratio; 
    cfg.m_hwp->set_register_value(cfg.m_modbus_address, static_cast<Tango::DevUShort>(tmp));
    break;
    }
  case Tango::DEV_ULONG :
    {
    Tango::DevULong src;
    attr.get_write_value(src);
    double tmp = static_cast<double>(src) / cfg.m_ratio; 
    cfg.m_hwp->set_register_value(cfg.m_modbus_address, static_cast<Tango::DevUShort>(tmp));
    break;
    }
  case Tango::DEV_FLOAT :
    {
    Tango::DevFloat src;
    attr.get_write_value(src);
    src /= cfg.m_ratio; 
    cfg.m_hwp->set_register_value(cfg.m_modbus_address, static_cast<Tango::DevUShort>(src));
    break;
    }
  case Tango::DEV_DOUBLE :
    {
    Tango::DevDouble src;
    attr.get_write_value(src);
    src /= cfg.m_ratio; 
    cfg.m_hwp->set_register_value(cfg.m_modbus_address, static_cast<Tango::DevUShort>(src));
    break;
    }
  default : 
    break;
  }
}

} // namespace mdv

