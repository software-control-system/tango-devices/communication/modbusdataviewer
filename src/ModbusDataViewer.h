//=============================================================================
//
// file :        ModbusDataViewer.h
//
// description : Include for the ModbusDataViewer class.
//
// project :	ModbusDataViewer
//
// $Author: vince_soleil $
//
// $Revision: 1.4 $
//
// $Log: not supported by cvs2svn $
// Revision 1.3  2010/03/23 08:55:31  jean_coquet
// work on progress - changing the Data property
//
// Revision 1.2  2010/03/22 16:54:15  jean_coquet
// work on progress
// test with Modbus Slave simulation programm
//
//
// copyleft :    European Synchrotron Radiation Facility
//               BP 220, Grenoble 38043
//               FRANCE
//
//=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================
#ifndef _MODBUSDATAVIEWER_H
#define _MODBUSDATAVIEWER_H

#include <tango.h>
#include "DynamicAttributesHelper.h"
#include "task/HWProxy.h"
//using namespace Tango;
#include <yat4tango/DeviceInfo.h>
#include <yat4tango/Logging.h>

/**
 * @author	$Author: vince_soleil $
 * @version	$Revision: 1.4 $
 */

 //	Add your own constants definitions here.
 //-----------------------------------------------


namespace ModbusDataViewer_ns
{

/**
 * Class Description:
 * generic device to get/set single coils and registers in a modbus device
 *	uses dynamic attributes
 */

/*
 *	Device States Description:
 */


class ModbusDataViewer: public Tango::Device_4Impl
{
public :
	//	Add your own data members here
	//-----------------------------------------


	//	Here is the Start of the automatic code generation part
	//-------------------------------------------------------------	
/**
 *	@name attributes
 *	Attributs member data.
 */
//@{
//@}

/**
 *	@name Device properties
 *	Device properties member data.
 */
//@{
/**
 *	list of the data to be displayed
 *	1 line by data
 *	for each line the device will create 1 dynamic attribute
 *	Format :
 *	AttributeName;DataType;Data Access;Modbus Address;Ratio;Read Register Type
 *	Example :
 *	Temperature;DEV_DOUBLE;READ_WRITE;5;0.1
 *	Prop;DEV_DOUBLE;READ_WRITE;35;1.0
 *	
 *	Attribute Name = the name of the Attribute
 *	Data Type = type of the Attribute [DEV_BOOLEAN|DEV_SHORT|DEV_USHORT|DEV_LONG|DEV_ULONG|DEV_FLOAT|DEV_DOUBLE]
 *	Data Access  = [READ|WRITE|READ_WRITE]
 *	Modbus Address = modbus Address see Modbus reference [0...10000]
 *	Ratio = floating point, if omitted defaults to 1.0
 *	Read Register Type = [HOLDING|INPUT] if omitted or error defaults to HOLDING
 *	
 */
	vector<string>	data;
/**
 *	Modbus Device name
 */
	string	url;
/**
 *	reading period for the thread
 */
	Tango::DevLong	hardwarePollingTime;
/**
 *	Number of connection attempts.
 *	-1 is for infinite number of connection attempts
 */
	Tango::DevShort	nbConnectionRetry;
//@}

/**@name Constructors
 * Miscellaneous constructors */
//@{
/**
 * Constructs a newly allocated Command object.
 *
 *	@param cl	Class.
 *	@param s 	Device Name
 */
	ModbusDataViewer(Tango::DeviceClass *cl,string &s);
/**
 * Constructs a newly allocated Command object.
 *
 *	@param cl	Class.
 *	@param s 	Device Name
 */
	ModbusDataViewer(Tango::DeviceClass *cl,const char *s);
/**
 * Constructs a newly allocated Command object.
 *
 *	@param cl	Class.
 *	@param s 	Device name
 *	@param d	Device description.
 */
	ModbusDataViewer(Tango::DeviceClass *cl,const char *s,const char *d);
//@}

/**@name Destructor
 * Only one desctructor is defined for this class */
//@{
/**
 * The object desctructor.
 */	
	~ModbusDataViewer() {delete_device();};
/**
 *	will be called at device destruction or at init command.
 */
	void delete_device();
//@}

	
/**@name Miscellaneous methods */
//@{
/**
 *	Initialize the device
 */
	virtual void init_device();
/**
 *	Always executed method befor execution command method.
 */
	virtual void always_executed_hook();

//@}

/**
 * @name ModbusDataViewer methods prototypes
 */

//@{
/**
 * This command gets the device state (stored in its <i>device_state</i> data member) and returns it to the caller.
 *	@return	State Code
 *	@exception DevFailed
 */
	virtual Tango::DevState	dev_state();
/**
 * This command gets the device status (stored in its <i>device_status</i> data member) and returns it to the caller.
 *	@return	Status description
 *	@exception DevFailed
 */
	virtual Tango::ConstDevString	dev_status();

/**
 *	Read the device properties from database
 */
	 void get_device_property();
//@}

	//	Here is the end of the automatic code generation part
	//-------------------------------------------------------------	



protected :	
	//	Add your own data members here
	//-----------------------------------------

  bool init_device_done;
  bool properties_missing;
  bool comm_error;
  std::string status_str;
  std::string last_error;

  //- he communication thread
	yat::SharedPtr<mdv::HWProxy> hwp;


  //- Dynamic Attributes
  mdv::DynamicAttributesHelper m_dyn_attrs;
  void add_dynamic_attribute (mdv::DynamicModbusAttribute::Config conf);
  void rem_dynamic_attributes (void);

  //- dynamic Attributes creation
  void create_dynamic_attributes (void);


};

}	// namespace_ns

#endif	// _MODBUSDATAVIEWER_H
