//=============================================================================
// DynamicAttributesHelper.cpp
//=============================================================================
// abstraction.......SOLEIL Motorized Insertion Device
// class.............DynamicAttributesHelper
// original author...N.Leclercq - SOLEIL
//=============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "DynamicAttributes.h"
#include "DynamicAttributesHelper.h"

namespace mdv
{

// ============================================================================
// DynamicAttributesHelper::ctor
// ============================================================================
DynamicAttributesHelper::DynamicAttributesHelper ()
	: host_device_(0)
{
  //- noop
}

// ============================================================================
// DynamicAttributesHelper::dtor
// ============================================================================
DynamicAttributesHelper::~DynamicAttributesHelper ()
{
  remove_all();
}

// ============================================================================
// DynamicAttributesHelper::dtor
// ============================================================================
void DynamicAttributesHelper::host_device (Tango::DeviceImpl * _host_device)
{
  host_device_ = _host_device;
}
  
// ============================================================================
// DynamicAttributesHelper::add 
// ============================================================================
void DynamicAttributesHelper::add (DynamicModbusAttribute * _attr)
{
	if (! host_device_)
	{
	  THROW_DEVFAILED("PROGRAMMING_ERROR",
	                  "no associated Tango device (host device not set)",
                    "DynamicAttributesHelper::add");
  }
                   
  //- check attribute does not already exist
  DynAttrIt it = rep_.find(_attr->get_name());
  if (it != rep_.end())
  {
	  THROW_DEVFAILED("OPERATION_NOT_ALLOWED",
	                  "attribute already exists",
                    "DynamicAttributesHelper::add");
  }

  //- add it to the device
  try
  {
    host_device_->add_attribute( _attr );
  }
  catch(Tango::DevFailed& ex)
  {
	  RETHROW_DEVFAILED(ex,
                      "INTERNAL_ERROR",
	                    "attribute could not be added to the device",
                      "DynamicAttributesHelper::add");
  }
  catch(...)
  {
	  THROW_DEVFAILED("UNKNOWN_ERROR",
	                  "unknown caught while trying to add attribute to the device",
                    "DynamicAttributesHelper::add");
  }
  
  //- ok, everything went fine :
  //- insert the attribute into the list
  std::pair<DynAttrIt, bool> insertion_result;
  insertion_result = rep_.insert( DynAttrEntry(_attr->get_name(), _attr) );

  if (insertion_result.second == false)
  {
	  THROW_DEVFAILED("OPERATION_NOT_ALLOWED",
	                  "attribute could not be inserted into the attribute repostory",
                    "DynamicAttributesHelper::add");
  }
}

// ============================================================================
// DynamicAttributesHelper::remove 
// ============================================================================
void DynamicAttributesHelper::remove (const std::string& _name)
{
	if (! host_device_)
	{
	  THROW_DEVFAILED("PROGRAMMING_ERROR",
	                  "no associated Tango device (host device not set)",
                    "DynamicAttributesHelper::remove");
  }
  
  //- check if attribute exists
  DynAttrIt it = rep_.find(_name);
  if (it == rep_.end())
  {
	  THROW_DEVFAILED("OPERATION_NOT_ALLOWED",
	                  "attribute does not exist",
                    "DynamicAttributesHelper::remove");
  }

  //- remove it from the device
  try
  {
    host_device_->remove_attribute( (*it).second, true );
  }
  catch(Tango::DevFailed& ex)
  {
	  RETHROW_DEVFAILED(ex,
                      "INTERNAL_ERROR",
	                    "attribute could not be removed from the device",
                      "DynamicAttributesHelper::remove");
  }
  catch(...)
  {
	  THROW_DEVFAILED("UNKNOWN_ERROR",
	                  "could not remove attribute from the device",
                    "DynamicAttributesHelper::remove");
  }

  //- remove from db
  Tango::DeviceData argin;
  std::vector<std::string> v(2);
  v[0] = host_device_->name();
  v[1] = _name;
  argin << v;

  Tango::Database * db = host_device_->get_db_device()->get_dbase();

  try
  {
    Tango::DeviceData argout = db->command_inout("DbDeleteDeviceAttribute", argin);
  }
  catch(Tango::DevFailed& ex)
  {
	  RETHROW_DEVFAILED(ex,
                      "INTERNAL_ERROR",
	                    "unable to delete attribute from the database",
                      "DynamicAttributesHelper::remove");
  }
  catch(...)
  {
	  THROW_DEVFAILED("UNKNOWN_ERROR",
	                  "unable to delete attribute from the database",
                    "DynamicAttributesHelper::remove");
  }

  //- remove from the internal map
  rep_.erase(it);
}

// ============================================================================
// DynamicAttributesHelper::remove_all 
// ============================================================================
void DynamicAttributesHelper::remove_all ()
{
	if (! host_device_ || rep_.empty())
		return;
  
  DynAttrIt it;

  Tango::DeviceData argin;
  std::vector<std::string> v(2);
  v[0] = host_device_->name();

  for (it  = rep_.begin(); it != rep_.end(); ++it)
  {

    //- remove it from the device
    try
    {
      host_device_->remove_attribute( (*it).second, true );
    }
    catch(Tango::DevFailed& ex)
    {
	    RETHROW_DEVFAILED(ex,
                        "INTERNAL_ERROR",
	                      "attribute could not be removed from the device",
                        "DynamicAttributesHelper::remove_all");
    }
    catch(...)
    {
	    THROW_DEVFAILED("UNKNOWN_ERROR",
	                    "could not remove attribute from the device",
                      "DynamicAttributesHelper::remove_all");
    }

    //- remove from db
    v[1] = (*it).first;
    argin << v;
    Tango::Database * db = host_device_->get_db_device()->get_dbase();

    try
    {
      Tango::DeviceData argout = db->command_inout("DbDeleteDeviceAttribute", argin);
    }
    catch(Tango::DevFailed& ex)
    {
	    RETHROW_DEVFAILED(ex,
                        "INTERNAL_ERROR",
	                      "unable to delete attribute from the database",
                        "DynamicAttributesHelper::remove_all");
    }
    catch(...)
    {
	    THROW_DEVFAILED("UNKNOWN_ERROR",
	                    "unable to delete attribute from the database",
                      "DynamicAttributesHelper::remove_all");
    }
  }

  //- then clear the map
  rep_.clear();
}

// ============================================================================
// DynamicAttributesHelper::begin
// ============================================================================
DynAttrCIt DynamicAttributesHelper::begin() const
{
  return rep_.begin();
}

// ============================================================================
// DynamicAttributesHelper::begin
// ============================================================================
DynAttrIt DynamicAttributesHelper::begin()
{
  return rep_.begin();
}

// ============================================================================
// DynamicAttributesHelper::end
// ============================================================================
DynAttrCIt DynamicAttributesHelper::end() const
{
  return rep_.end();
}

// ============================================================================
// DynamicAttributesHelper::end
// ============================================================================
DynAttrIt DynamicAttributesHelper::end()
{
  return rep_.end();
}

// ============================================================================
// DynamicAttributesHelper::size
// ============================================================================
size_t DynamicAttributesHelper::size() const
{
  return rep_.size();
}

// ============================================================================
// DynamicAttributesHelper::empty
// ============================================================================
bool DynamicAttributesHelper::empty() const
{
  return rep_.empty();
}

} // namespace

