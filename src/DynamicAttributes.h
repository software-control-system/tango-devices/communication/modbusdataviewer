//=============================================================================
// DynamicAttributes.h
//=============================================================================
// abstraction.......Modbus data generic access
// class.............DynamicAttribute
// original author...N.Leclercq - SOLEIL modifié par j.coquet
//=============================================================================

#ifndef _MDV_DYNAMIC_ATTRS_H_
#define _MDV_DYNAMIC_ATTRS_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <tango.h>
#include "task/HWProxy.h"

namespace mdv
{

// ============================================================================
// FORWARD DECLARATIONs
// ============================================================================
//- class HWProxy;

// ============================================================================
// Class DynamicAttribute
// ============================================================================
class DynamicAttribute : public Tango::LogAdapter
{
protected:
  //- ctor
  DynamicAttribute (yat::SharedPtr<HWProxy> hwp, Tango::DeviceImpl * host);

  //- ctor
  virtual ~DynamicAttribute ();  

  //- the insertion device
  yat::SharedPtr<HWProxy> m_hwp; 

private:
  //- not allowed method and operators
  DynamicAttribute (const DynamicAttribute&);
  const DynamicAttribute & operator= (const DynamicAttribute &);
};

// ============================================================================
// class: DynamicModbusAttribute
// ============================================================================
class DynamicModbusAttribute : public DynamicAttribute, public Tango::Attr
{
public:

  //- ctor argument
  struct Config
  {
    //- hardware proxy
    yat::SharedPtr<HWProxy> m_hwp;
    //- DevImpl of the device for internal Tango stuff
    Tango::DeviceImpl * m_host;
    //- dynamic Attribute Name
    std::string m_name;
    //- Attribute type, Tango type [DEV_BOOLEAN|DEV_SHORT|DEV_USHORT|DEV_LONG|DEV_ULONG|DEV_FLOAT|DEV_DOUBLE]
    long m_type;
    //- attribute access type (READ, WRITE, READ_WRITE)
    Tango::AttrWriteType m_rw;
    //- modbus Address
    Tango::DevShort m_modbus_address;
    //- ratio to be applied (by default : 1.0)
    double m_ratio;
    //- register type [INPUT|HOLDING] (by default : HOLDING)
    int m_register_type;
    //- number of connection attempts
    int m_nb_connection_retry;

		//- Ctor -------------
		Config ();

		//- Ctor -------------
		Config ( const Config & _src);

		//- operator = -------
		void operator = (const Config & src);
  };
  
	//- ctor
	DynamicModbusAttribute (const Config & cfg);

	//- dtor
	virtual ~DynamicModbusAttribute ();

  //- read
	virtual void read(Tango::DeviceImpl *dev, Tango::Attribute &att);

  //- write
	virtual void write(Tango::DeviceImpl *dev, Tango::WAttribute &att);

  //- is_allowed
  virtual bool is_allowed (Tango::DeviceImpl *dev, Tango::AttReqType ty)
  {
    return true;
  }
  
private:
  //- not allowed method and oparators
  DynamicModbusAttribute ();
  DynamicModbusAttribute (const DynamicModbusAttribute&);
  const DynamicModbusAttribute& operator= (const DynamicModbusAttribute&);
  Config cfg;

  //- the attribute variables
  Tango::DevUShort tmp;
  Tango::DevBoolean db;
  Tango::DevShort ds;
  Tango::DevUShort dus;
  Tango::DevLong dl;
  Tango::DevULong dul;
  Tango::DevFloat df;
  Tango::DevDouble dd;

  };


} // namespace mdv

#endif //- _MDV_DYNAMIC_ATTRS_H_

