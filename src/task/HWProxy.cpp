
//- Project : generic thread for access to the Modbus Devices
//- file : HWProxy.cpp
//- Read/writes coils and registers on a modbus device

#include "task/HWProxy.h"
#include <math.h>
#include <yat/threading/Mutex.h>

namespace mdv
{

// ============================================================================
// some defines enums and constants
// ============================================================================
  const double __NAN__ = ::sqrt (-1.);

// ============================================================================
// HWProxy_ns::Config::operator=
// ============================================================================
  
  void HWProxy::Config::operator= (const Config & src)
  {
    url = src.url;
    periodic_timeout_ms = src.periodic_timeout_ms;
  }

// ============================================================================
// HWProxy_ns::Config::Config ()
// ============================================================================
  
  HWProxy::Config::Config ()
  {
    url = "";
    periodic_timeout_ms = 1000;
  }

  //-----------------------------------------------
  //- Ctor ----------------------------------------
  HWProxy::HWProxy (Tango::DeviceImpl * _host_device,
                    const HWProxy::Config & _conf): 
                  yat4tango::DeviceTask(_host_device),
                  host_dev (_host_device),
                  conf(_conf) 

  {
    DEBUG_STREAM << "HWProxy::HWProxy <- " << std::endl;

    //- yat::Task configure optional msg handling
    enable_timeout_msg(false);
    enable_periodic_msg(true);
    set_periodic_msg_period(conf.periodic_timeout_ms);
    mbus = 0;
  }

  //-----------------------------------------------
  //- Dtor ----------------------------------------

  HWProxy::~HWProxy (void)
  {
    DEBUG_STREAM << "HWProxy::~HWProxy <- " << std::endl;
  }

  //-----------------------------------------------
  //- the user core of the Task -------------------
  void HWProxy::process_message (yat::Message& _msg)
  {
    //- The DeviceTask's lock_ -------------

    DEBUG_STREAM << "HWProxy::process_message::receiving msg " << _msg.to_string() << std::endl;
 
    //- handle msg
    switch (_msg.type())
    {
      //- THREAD_INIT =======================
    case yat::TASK_INIT:
      {
        DEBUG_STREAM << "HWProxy::handle_message::THREAD_INIT::thread is starting up" << std::endl;
        //- "initialization" code goes here
        try
        {
          mbus = 0;
          DEBUG_STREAM << " HWProxy::handle_message handling TASK_INIT try to get device proxy on  " << conf.url << std::endl;
          com_error = 0;
          consecutive_com_errors = 0;
          com_success = 0;
          mbus = new Tango::DeviceProxy (conf.url);
          mbus->ping();

          com_state =HWP_NO_ERROR;
        }
        catch (std::bad_alloc)
        {
          ERROR_STREAM << "HWProxy::process_message::TASK_INIT OUT_OF_MEMORY cannot allocate device proxy" << std::endl;
          last_error = "HWProxy::HWProxy OUT_OF_MEMORY cannot allocate device proxy ";
          com_state =HWP_SOFTWARE_ERROR;
          return;
        }

        catch(...)
        {
          ERROR_STREAM << " HWProxy::process_message::TASK_INIT exception caugth trying to get device proxy on " << conf.url << std::endl;
          last_error = " exception caugth trying to get device proxy on " + conf.url;
          com_state =HWP_DEVICE_PROXY_ERROR;
          return;
        }
      } 
      break;
      //- TASK_EXIT =======================
    case yat::TASK_EXIT:
      {
        DEBUG_STREAM << "HWProxy::handle_message handling TASK_EXIT thread is quitting" << std::endl;

        //- "release" code goes here
        if (mbus)
        {
          delete mbus;
          mbus = 0;
        }
      }
      break;
      //- TASK_PERIODIC ===================
    case yat::TASK_PERIODIC:
      {
        DEBUG_STREAM << "HWProxy::handle_message handling TASK_PERIODIC msg" << std::endl;
        //- code relative to the task's periodic job goes here
        { 
          //- enter critical section 
          //- yat::AutoMutex<> guard (m_lock);
          //- TODO check if the modbus proxy is healthy
        }
      }
      break;


      //- TASK_TIMEOUT ===================
    case yat::TASK_TIMEOUT:
      {
        //- code relative to the task's tmo handling goes here
        DEBUG_STREAM << "HWProxy::handle_message handling TASK_TIMEOUT msg" << std::endl;
      }
      break;
      //- USER_DEFINED_MSG ================
    } //- switch (_msg.type())
    
  } //- HWProxy::handle_message



  //- bool Accessor ------------------------
  bool HWProxy::get_bool_value (Tango::DevShort modbus_address)
  {
    DEBUG_STREAM << "HWProxy::get_bool_value <-" << std::endl;
    Tango::DevShort res = 0;
    try
    {
      Tango::DeviceData ddin, ddout;
      ddin << modbus_address;
      { //- enter critical section
        yat::AutoMutex<> guard(m_lock);
        if(mbus)
        {
          ddout = mbus->command_inout("ReadCoilStatus", ddin);
          ddout >> res;
        }
      }
    }
    catch (Tango::DevFailed &e)
    {
      ERROR_STREAM << "HWProxy::get_bool_value Modbus Address [" << modbus_address << "] failed caugth DevFailed \n" << e << std::endl;     
      com_error ++;
      consecutive_com_errors ++;
      com_state = HWP_COMMUNICATION_ERROR;
      last_error = "\nlast error : DevFailed exception occured trying to read Modbus Proxy\n";
	    last_error += static_cast<const char *>(e.errors[0].desc);
	    com_status = "\nDevFailed exception occured trying to read Modbus Proxy";
      throw (e);
    }
    catch (...)
    {
      ERROR_STREAM << "HWProxy::get_bool_value Modbus Address [" << modbus_address << "] failed caugth (...)" << std::endl;     
      com_error ++;
      consecutive_com_errors ++;
      com_state = HWP_COMMUNICATION_ERROR;
      last_error = "\nlast error : exception occured trying to read Modbus Proxy";
      com_status = "(...) exception occured trying to read Modbus Proxy";
      Tango::Except::throw_exception (static_cast<const char *> ("COMMUNICATION_ERROR"),
                                      static_cast<const char *> ("(...) exception occured trying to read Modbus Proxy"),
                                      static_cast<const char *> ("HWProxy::get_bool_value"));
    }

    if(mbus)
    {
      DEBUG_STREAM << "HWProxy::get_bool_value successfull " << std::endl;
      com_state = HWP_NO_ERROR;
      com_status = "communication with Modbus Device is up and running";
      com_success ++;
      consecutive_com_errors = 0;
    }

    return (res != 0? true : false);
  }
  //- register Accessor ------------------------
  Tango::DevUShort HWProxy::get_register_value (Tango::DevShort modbus_address, Tango::DevShort register_type)
  {
    DEBUG_STREAM << "HWProxy::get_register_value for Modbus Address [" << modbus_address << "] <-" << std::endl;
    //- register type stuff
    register_type = register_type != 0 ? 1 : 0;
    static std::string modbus_cmd [] = 
    {
      "ReadHoldingRegisters",
      "ReadInputRegisters"
    };

    std::vector <Tango::DevShort> res;
    try
    {
      //- read register(s) starting at modbus_address 1 register 
      std::vector<short> input;
		  input.push_back(modbus_address);
		  input.push_back(1);
      Tango::DeviceData ddin, ddout;
      ddin << input;
      { //- enter critical section
        yat::AutoMutex<> guard(m_lock);
        //- TODO : test if ReadHoldingRegisters does the same or access to a different zone in the modbus device
        if(mbus)
        {
          ddout = mbus->command_inout(modbus_cmd[register_type], ddin);
          ddout >> res;
        }
      }
      if (res.size () < 1)
      Tango::Except::throw_exception (static_cast<const char *> ("COMMUNICATION_ERROR"),
                                      static_cast<const char *> ("response is empty, DevVarShortArray length < 1"),
                                      static_cast<const char *> ("HWProxy::get_register_value"));


      DEBUG_STREAM << "HWProxy::get_register_value Modbus Address [" << modbus_address << "] value = <" << res[0] << ">" << std::endl;
    }
    catch (Tango::DevFailed &e)
    {
      ERROR_STREAM << "HWProxy::get_register_value Modbus Address [" << modbus_address << "] failed caugth DevFailed \n" << e << std::endl;     
      com_error ++;
      consecutive_com_errors ++;
      com_state = HWP_COMMUNICATION_ERROR;
      last_error = "\nlast error : DevFailed exception occured trying to read Modbus Proxy\n";
	    last_error += static_cast<const char *>(e.errors[0].desc);
	    com_status = "\nDevFailed exception occured trying to read Modbus Proxy";
      throw (e);
    }
    catch (...)
    {
      ERROR_STREAM << "HWProxy::get_register_value Modbus Address [" << modbus_address << "] failed caugth (...)" << std::endl;     
      com_error ++;
      consecutive_com_errors ++;
      com_state = HWP_COMMUNICATION_ERROR;
      last_error = "\nlast error : exception occured trying to read Modbus Proxy";
      com_status = "(...) exception occured trying to read Modbus Proxy";
      Tango::Except::throw_exception (static_cast<const char *> ("COMMUNICATION_ERROR"),
                                      static_cast<const char *> ("(...) exception occured trying to read Modbus Proxy"),
                                      static_cast<const char *> ("HWProxy::get_register_value"));
    }

    if(mbus)
    {
      DEBUG_STREAM << "HWProxy::get_register_value successfull " << std::endl;
      com_state = HWP_NO_ERROR;
      com_status = "communication with Modbus Device is up and running";
      com_success ++;
      consecutive_com_errors = 0;
    }
    return static_cast <Tango::DevUShort>(res[0]);
  }


  //- bool Mutator ------------------------
  void HWProxy::set_bool_value (Tango::DevShort modbus_address, bool val)
  {
    DEBUG_STREAM << "HWProxy::set_bool_value <-" << std::endl;

    std::vector<short> input;
	  input.push_back(modbus_address);
    if (val)
      input.push_back(1);
    else
      input.push_back(0);
    Tango::DeviceData ddin;
    ddin << input;

    try
    {
      { //- enter critical section
        yat::AutoMutex<> guard(m_lock);
        mbus->command_inout("ForceSingleCoil", ddin);
      }
    }
    catch (Tango::DevFailed &e)
    {
      ERROR_STREAM << "HWProxy::set_bool_value Modbus Address [" << modbus_address << "] failed caugth DevFailed \n" << e << std::endl;     
      com_error ++;
      consecutive_com_errors ++;
      com_state = HWP_COMMUNICATION_ERROR;
      last_error = "\nlast error : DevFailed exception occured trying to write on  Modbus Proxy\n";
	    last_error += static_cast<const char *>(e.errors[0].desc);
	    com_status = "\nDevFailed exception occured trying to write on Modbus Proxy";
      throw (e);
    }
    catch (...)
    {
      ERROR_STREAM << "HWProxy::set_bool_value Modbus Address [" << modbus_address << "] failed caugth (...)" << std::endl;     
      com_error ++;
      consecutive_com_errors ++;
      com_state = HWP_COMMUNICATION_ERROR;
      last_error = "\nlast error : exception occured trying to write on Modbus Proxy";
      com_status = "(...) exception occured trying to write on Modbus Proxy";
      Tango::Except::throw_exception (static_cast<const char *> ("COMMUNICATION_ERROR"),
                                      static_cast<const char *> ("(...) exception occured trying to read Modbus Proxy"),
                                      static_cast<const char *> ("HWProxy::set_bool_value"));

    }
    DEBUG_STREAM << "HWProxy::set_bool_value successfull " << std::endl;
    com_state = HWP_NO_ERROR;
    com_status = "communication with Modbus Device is up and running";
    com_success ++;
    consecutive_com_errors = 0;
  }


  //- register Mutator ------------------------
  void HWProxy::set_register_value (Tango::DevShort modbus_address, Tango::DevUShort value)
  {
    DEBUG_STREAM << "HWProxy::set_register_value <-" << std::endl;
    try
    {
      //- write register(s) starting at modbus_address 1 register 
      std::vector<short> input;
		  input.push_back(modbus_address);
      input.push_back(static_cast<Tango::DevShort> (value));
      Tango::DeviceData ddin;
      ddin << input;
      { //- enter critical section
        yat::AutoMutex<> guard(m_lock);
        mbus->command_inout("PresetSingleRegister", ddin);
      }
    }
    catch (Tango::DevFailed &e)
    {
      ERROR_STREAM << "HWProxy::set_register_value Modbus Address [" << modbus_address << "] failed caugth DevFailed \n" << e << std::endl;     
      com_error ++;
      consecutive_com_errors ++;
      com_state = HWP_COMMUNICATION_ERROR;
      last_error = "\nlast error : DevFailed exception occured trying to write on Modbus Proxy\n";
	    last_error += static_cast<const char *>(e.errors[0].desc);
	    com_status = "\nDevFailed exception occured trying to write on Modbus Proxy";
      throw (e);
    }
    catch (...)
    {
      ERROR_STREAM << "HWProxy::set_register_value Modbus Address [" << modbus_address << "] failed caugth (...)" << std::endl;     
      com_error ++;
      consecutive_com_errors ++;
      com_state = HWP_COMMUNICATION_ERROR;
      last_error = "\nlast error : exception occured trying to write on Modbus Proxy";
      com_status = "(...) exception occured trying to write on Modbus Proxy";
      Tango::Except::throw_exception (static_cast<const char *> ("COMMUNICATION_ERROR"),
                                      static_cast<const char *> ("(...) exception occured trying to read Modbus Proxy"),
                                      static_cast<const char *> ("HWProxy::set_register_value"));
    }
    DEBUG_STREAM << "HWProxy::get_register_value successfull " << std::endl;
    com_state = HWP_NO_ERROR;
    com_status = "communication with Modbus Device is up and running";
    com_success ++;
    consecutive_com_errors = 0;
  }

} //- namespace
