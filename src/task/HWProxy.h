//------------------------------------------------------------------------
//- Project : generic thread for access to the Modbus Devices
//- file : HWProxy.h
//- Read/writes coils and registers on a modbus device
//- TODO : get the data at the modbus address in the TASK_PERIODIC 
//------------------------------------------------------------------------


#ifndef __HW_PROXY_H__
#define __HW_PROXY_H__

#include <tango.h>
#include <yat4tango/DeviceTask.h>


namespace mdv
{
  // ============================================================================
  // some defines enums and constants
  // ============================================================================
  //- startup and communication errors
  const size_t MAX_COM_RETRIES = 20;
  typedef enum
  {
    HWP_UNKNOWN_ERROR        = 0,
    HWP_INITIALIZATION_ERROR,
    HWP_DEVICE_PROXY_ERROR,
    HWP_COMMUNICATION_ERROR,
    HWP_HARDWARE_ERROR,
    HWP_SOFTWARE_ERROR,
    HWP_NO_ERROR
  } ComState;
  const size_t HWP_STATE_MAX_SIZE = 7;

  static const std::string hw_state_str[HWP_STATE_MAX_SIZE] =
  {
    "Unknown Error",
    "Initialisation Error",
    "Device Proxy Error",
    "Communication Error",
    "Hardware Error",
    "Software Error",
    "Communication Running"
  };

  //---------------------------------------------------------  
  //- the YAT user messages 


  //------------------------------------------------------------------------
  //- HWProxy Class
  //- read /write in the modbus device with the help of DeviceProxy
  //------------------------------------------------------------------------
  class HWProxy : public yat4tango::DeviceTask
  {

  //- create a dedicated type for HWProxy configuration
  //---------------------------------------------------------
    public :
  typedef struct Config
  {
    //- members --------------------
    std::string url;
    size_t periodic_timeout_ms;

    //- ctor -----------------------
    Config ();
    //- operator= ------------------
    void operator= (const Config& src);
  } Config;

    //- Constructeur/destructeur
      HWProxy (Tango::DeviceImpl * _host_device,
               const Config & conf);

    virtual ~HWProxy ();

	  //- bool Accessor ------------------------
    bool get_bool_value (Tango::DevShort modbus_address);
	  //- bool Mutator -------------------------
    void set_bool_value (Tango::DevShort modbus_address, bool value);
	  //- Register Accessor --------------------
    Tango::DevUShort get_register_value (Tango::DevShort modbus_address, Tango::DevShort register_zone);
	  //- Register Mutator ---------------------
    void set_register_value (Tango::DevShort modbus_address, Tango::DevUShort value);


    //- the number of communication successfully done
    unsigned long get_com_error (void)   { return com_error; };

    //- the number of communication errors
    unsigned long get_com_success (void) { return com_success; };

    //- the state and status of communication
    ComState get_com_state (void)        {return com_state;};
    std::string get_com_status (void)    { return com_status ; };
    std::string get_last_error (void)    { return last_error ; };
    
  protected:
	  //- process_message (implements yat4tango::DeviceTask pure virtual method)
	  virtual void process_message (yat::Message& msg);


   private :
   
   //- utilities
   yat::Mutex m_lock;

    //- the configuration
    Config conf;

    //- Tango communication device stuff
    Tango::DeviceProxy * mbus;

    std::string m_com_dev_name;
    std::string m_dev_name;
    //- the host device 
    Tango::DeviceImpl * host_dev;


    //- the state and status stuff
    ComState com_state;
    std::string com_status;

    std::string last_error;
    //- for Com error attribute
    unsigned long com_error;
    //- for Com OK attribute
    unsigned long com_success;

    //- internal error counter
    unsigned int consecutive_com_errors;
  };



}
#endif //- __HW_PROXY_H__
