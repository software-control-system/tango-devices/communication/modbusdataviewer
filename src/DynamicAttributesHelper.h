//=============================================================================
// DynamicAttributesHelper.h
//=============================================================================
// abstraction.......Modbus Data Viewer 
// class.............DynamicAttributesHelper
// original author...N.Leclercq - SOLEIL
//=============================================================================

#ifndef _DYNAMIC_ATTRS_HELPER_H_
#define _DYNAMIC_ATTRS_HELPER_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "DynamicAttributes.h"
#include <yat4tango/ExceptionHelper.h>
namespace mdv
{

typedef std::map <std::string, DynamicModbusAttribute*> DynAttrRepository;
typedef DynAttrRepository::value_type        DynAttrEntry;
typedef DynAttrRepository::iterator          DynAttrIt;
typedef DynAttrRepository::const_iterator    DynAttrCIt;

// ============================================================================
// class: DynamicAttributesHelper
// ============================================================================
class DynamicAttributesHelper
{
public:
  /**
   * Constructor. 
   * @param  _host_device the device handled by the instance
   */
  DynamicAttributesHelper();
  
  /**
   * Destructor
   */
  ~DynamicAttributesHelper();

  /**
   * host_device
   * @param Tango device hosting the <DAH> instance
   */
  void host_device (Tango::DeviceImpl * host_device);
    
  /**
   * add
   * @param attr a Tango::Attr* to be registered
   */
  void add (DynamicModbusAttribute * attr);

  /**
   * remove
   * @param name the attribute name
   */
  void remove (const std::string& name);

  /**
   * remove_all
   *
   * Removes all the dynamic attributes registered
   */
  void remove_all ();

  /**
   * get (Tango::Attr version)
   * @param _name the attribute name
   * @param _a a reference to a Tango::Attr* where the pointer to the desired attribute is stored
   */
  inline DynamicModbusAttribute * get (const std::string& _name)
  {
    DynAttrIt it = rep_.find(_name);
	  if (it == rep_.end())
	  {
	    THROW_DEVFAILED("INVALID_ARG",
	                    "attribute does not exist",
                      "DynamicAttributesHelper::get_attribute");
	  }
	  return (*it).second;
  }

  DynAttrCIt begin() const;
  DynAttrIt  begin();
  
  DynAttrCIt end() const;
  DynAttrIt  end();
  
  size_t size() const;
  bool empty() const;

private:
  Tango::DeviceImpl * host_device_;
  DynAttrRepository   rep_;

  DynamicAttributesHelper (const DynamicAttributesHelper&);
  DynamicAttributesHelper& operator= (const DynamicAttributesHelper&);
};

} // namespace

#endif 
